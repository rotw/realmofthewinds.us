+++
menu = "main"
title = "Rules"
type = "single"
plainIDAnchors = false
extensionsmask = ["headerIds"]
+++

<style>
    #main h1 {
        font-size: 36px;
    }
    #main h2 {
        font-size: 30px;
    }
    .row > ol, .row > ol > li > :first-child {
        font-size: 24px;
    }
    #main h3 {
        font-size: 18px;
    }

    .row > ol > li > * {
        font-size: 15px;
    }
    .row > ol p {
        margin: 0;
    }
    .row > ol ul {
        list-style-type: lower-alpha;
        padding-left: 25px;
    }
    .row > ol ul ul {
        list-style-type: lower-roman;
    }
    #c-administrators-and-head-moderator-a-class-anchor-id-staff-c-admin-a + ol, #c-administrators-and-head-moderator-a-class-anchor-id-staff-c-admin-a + ol > li > :first-child {
        font-size: 18px;
    }
</style>

# I. Chat Rules {{< anchor chat >}}
_Applies to all communication within our domain._


1. Treat everyone with respect. {{< anchor chat-1-respect >}}

    - No personal attacks; address the issue, not the person.
    - No derogatory language.
    - Do not provoke or continue heated discussions; take it outside.
    - Respect preferred names and pronouns.
        - If you use the wrong one by accident, correct yourself and/or apologize.
        - They/them is acceptable in any case.
        - It/that is never acceptable when referring to one or more person.
    - Respect all final decisions of staff members on any issue. If you feel a staff member has broken or contradicted a rule, file a report, and don't discuss it in public chat.

2. No verbal harassment. {{< anchor chat-2-harassment >}}

    - Do not annoy others with repeated messages or requests.
    - No trolling or flamebaiting.  
    *"Making a deliberately offensive or provocative online post with the aim of upsetting someone or eliciting an angry response from them." (Google: define trolling.)*

3. No spamming or advertising. {{< anchor chat-3-spam-ad >}}

    - Spamming is excessive/fast repetition of similar messages/characters/images.
    - Advertising is sharing a message or link intended to promote a product or service that is outside of our domain.
    - No excessive use of capital letters, formatting, emojis, or images.

4. Do not ask for or share personal information. {{< anchor chat-4-personal-info >}}

    - Personal information includes, but is not limited to: financial information, real name, phone numbers, photos, real world location, and links to social media or other websites containing any of this information.
    - Sharing or asking for sensitive financial information or photos of others can lead to being permanently banned without warning.

5. Do not circumvent punishments. {{< anchor chat-5-circumvent >}}

    Punishments may be applied to all accounts sharing the same IP address unless we have sufficient reason to believe the account is being used by a different individual.

6. Only write english in public chat. {{< anchor chat-6-english >}}

    *Exception:* Other languages may be used if it is relevant to the conversation for words to be in that language and the majority of the conversation is in english.

    ### Example
    *PlayerA: Today in spanish class, we learned how to say desarrolladores.*  
    *PlayerB: ¡Qué susto! That must have been muy difícil!*  
    *PlayerA: sí señor*

7. In addition to our rules, follow [Discord's Community Guidelines](https://discordapp.com/guidelines). {{< anchor chat-7-discord >}}

    - Our public game chat is linked with the *#minecraft* channel on our Discord.
    - If you violate the guidelines in Discord directly, you will be reported to Discord in addition to our normal punishments.

8. No inappropriate usernames or nicknames. {{< anchor chat-8-names >}}

    - Usernames that violate our other chat rules are forbidden.
    - Impersonation of an established player or staff member is forbidden.
    - Nicknames should be similar to your in-game name.  
    - No ugly name colors
        - No striped names.
        - No bold, obfuscated, underline, italics, or black.
        - No high-contrast colors.
    - If you refuse to change your name when asked or you are unable to:
        - In case of impersonation, a "FAKE" prefix will be given.
        - In other cases, you will be temp-banned for the number of days you have left before you can change your name plus three or a minimum of 5 days.

# II. Game Rules {{< anchor game >}}
_Applies to all game worlds._


1. No griefing on claimed land in any world. {{< anchor game-1-griefing >}}

    - Griefing is breaking or placing blocks without permission or in a destructive manner.
    - Killing mobs and/or stealing items is NOT griefing.
    - If you grief on accident and fix it or ask staff to help fix it, you won't be punished.

    ### Exceptions
    - Breaking and/or placing farmland crops.
    - Breaking other farmable blocks such as pumpkins, melons, cactus, sugarcane, cocoa beans, and chorus fruit.
    - Breaking and/or placing shulker boxes with intent to steal the shulker box and/or access the inventory inside.
    - Indirectly causing a block to be changed.  
    *Example: Holding a sheep attached to a lead over grass.*
    - Placing ladders with intent to be used for climbing and not any other reason.
    - You can take items out of frames, but not break item frames or paintings.

2. Follow property rules if you are a member of said property. {{< anchor game-2-property >}}

    - In case that a property rule conflicts with a server rule, the server rule has priority.
    - Property rules must be posted on clearly visible signs.
    - Property rules may designate areas that specific people/groups can or cannot access.
    - Property rules that restrict actions other than breaking or placing blocks (that aren't in the exceptions list) may be defined, but will not be enforced by server staff.
    - Property rules written on a sign can only affect players with the same or lesser role than that of the writer.
    - Effective ownership may be given to someone with a manager or greater role to a subarea of a property.
    - At staff discretion, verbal contracts designating specific permissions may also be enforced, but signs are greatly preferred.
    - When a grief of a designated area is reported, staff will check the following before making a decision:
        - The date/time of the grief and who did it.
        - The date/time of the signs being written and who wrote them.

3. Raiding is allowed and encouraged in survival worlds, but with a few restrictions to be fair. {{< anchor game-3-raiding >}}

    - Raiding is stealing items and/or killing mobs belonging to another player without permission.
    - If, at any point, one player griefs as defined in [Game Rule #1](#game-1-griefing), the entire raid is illegal and may be rolled back.
    - If a staff member asks you to return items as part of a rollback, you must do so. Staff will make it clear whether returning items is optional or not.
    - Don't remove more items from chests than you are willing to carry home. In other words, don't drop stolen items on the ground.
    - If you have killed every online player in a property, then a member of the property may ask to leave, and you must do so within an hour.
    - Do not return to the site of the attack with intent to raid or kill for at least 3 days after the initial attack.
    - If one or more property members are online and inside the property you are raiding and a staff member deems your actions unfair, then staff can require you to leave.

4. No hacked clients, cheat mods, or x-ray of any form. {{< anchor game-4-cheating >}}
    
    - Do not use mods that are not approved.
    - If a mod you want to use isn't on this list or you're not sure, ask a moderator.
    - Do not accuse someone of cheating in public. Concern about cheating should be reported to a staff member in private.

    ### Approved Mods
    - Optifine
    - Shaders
    - Resource packs that are not x-ray.
    - Minimap mods. (i.e. Rei's Minimap, Zan's Minimap, VoxelMap)
    - Mods that display more technical information but don't give you an advantage in PvP. (i.e. Chunk borders, Armor HUD, etc.)

5. Land claimed must be used. Structures abandoned for a long time may be deleted by staff. {{< anchor game-5-land-use >}}

    - For a structure to be considered abandoned, it must meet the following criteria:
        - All owners of the structure have been offline for at least a month.
        - It is in a state of disrepair or violates the building code outlined in [Game Rule #7](#game-7-build-rules).
        - It is not being used by anyone else with permission.
    - Structures can (and not necessarily will) be deleted with permission of all structure owners for any other reason.
    - The existence of a request for deletion does not, in any way, give anyone permission to grief it. Only admins are allowed to remove or reset structures.

6. Do not build near someone else without their permission. {{< anchor game-6-build-near >}}

    - Near means that it matches any of the following criteria:
        - Within visible distance
        - Attached to the same landmass
        - Above or below
    - If you build an underground base, it is your responsibility to check for builds above and to mark your land above ground with something obviously not natural.
    - If you build on or above sea level and someone built an underground base without marking it above ground, then you are not at fault.

7. Do not build offensive, destructive, or lag-inducing structures. {{< anchor game-7-build-rules >}}

    Builds should follow the global rules regarding offensive language.

    ### Building Rules
    - Don't build structures mostly or entirely of earthy materials such as dirt, grass, podzol, etc. unless it is a terraform.
    - Don't build excessively flashy structures.

    ### Redstone Rules
    - Redstone devices must not affect anything outside of your property.
    - Don't build redstone devices that cause client and/or server lag on average hardware.
    - XP grinders and redstone clocks must have an off-switch or otherwise limit how much they run. Don't forget to shut it off when it's not in use.
    - Spawner-based xp grinders do not have to have an off switch if they aren't in frequently trafficked areas.
    - Mob farms with an automated kill mechanism are not counted as xp grinders, and therefore don't need an off switch.
    - Don't build a mob farm or containment for boss mobs such as the wither.

8. Do not abuse glitches that aren't explicitly allowed. {{< anchor game-8-glitches >}}

    For glitches not on either list or if you aren't sure, ask an admin before using it.

    ### Allowed Glitches
    - Redstone-related glitches that don't create new items.
    - Bedrock-breaking glitches (if there still are any that Mojang didn't fix).
    - Obsidian generators.
    - Glitches that allow going through walls in vanilla Minecraft without lag.

    ### Illegal Glitches
    - Anything that involves breaking or placing blocks where you don't have access to.
    - Glitches involving lag.
    - Taking items out of menus that aren't intended to be movable.
    - Duplicating items.
    - Using enderpearls to teleport into or through blocks without a gap.

9. Don't attack staff unless you are prepared to be at a severe disadvantage. {{< anchor game-9-staff-pvp >}}

    - Staff are not allowed to participate in PvP/raiding except in defense.
    - Items will be returned.
    - Do not pick up items that were dropped by a moderator/admin without permission. Their inventory may have contained spawned-in items that you aren't allowed to keep.
    - Staff can pvp/raid with the permission of everyone involved.

<!-- 10. A shop must be active, or it may be evicted. {{< anchor game-10-shop >}}
    - The cost of shop plots is posted on the welcome board when entering the market.
    - Payments for plots must be made each month, and can be made in advance.
    - Shop plots must only be used as a shop, not storage, home, work area, etc.
    - If any of these rules are broken, one of the following actions may be taken depending on the number of recent infractions a shop recieved.
        - The plot owner or an active manager will be sent up to 3 warnings depending on severity.
        - The plot will be auctioned off to the highest bidder. The original owner/managers keep nothing.
        - In the event that the auction fails, the contents of the shop plot will be deleted.-->

# III. Staff Rules {{< anchor staff >}}
_This information is public so you know what to expect from us._
_See also: [Staff Ranks](/ranks/#staff)_

## A. Guides and up {{< anchor staff-a-guides >}}

1. Treat all players equally and fairly. {{< anchor staff-a1-fairness >}}

    - Respect all players.
    - Do not lie to players or other staff under any circumstances.
    - Do not use your powers to give yourself or anyone else an unfair advantage or break any player rules.
    - Staff powers cannot be used to enforce non-server rules unless one or more server rules are broken in the process.

2. Follow all player rules. {{< anchor staff-a2-player >}}

    - Staff are not above the law.
    - Infractions may result in demotion _in addition_ to regular punishments.

3. Do your part in enforcing rules. {{< anchor staff-a3-duty >}}

    - Staff duties come before any player duties including your guild or town. (Applies to alternate accounts as well.)
    - If you receive a report and you don't have the tools necessary to prove rules were broken, escalate this to the next staff rank up.
    - If you receive a report and a lower ranked staff member is available to solve this issue, deescalate this to the next staff rank down.

4. Handle player punishments in a fair and professional manner. {{< anchor staff-a4-punish >}}

    - Permanent Bans should only be given when a player is uncooperative and unlikely to reform.
    - Give at least one warning first.
    - For chat-related offenses, a temp-mute should follow a warning.
    - If you catch a player in the act of griefing, freeze them first and then warn them.
    - All bans, kicks, and warnings should include a reason.

## B. Moderators and up {{< anchor staff-b-mods >}}

1. Maintain a good attendance. {{< anchor staff-b1-attendance >}}

    - If you know you'll be unavailable for longer than a couple days, please let staff know.
    - If the admins and/or head moderator feel that you haven't been doing enough, you may be demoted.
    - Enable notifications for the following at minimum:
        - Mentions such as @YourName or @Staff and private messages.
        - New messages in the Information, Support, and Staff categories.
    - Respond promptly when necessary.

2. If you are or plan to become staff on another server, you should resign. {{< anchor staff-b2-staff-fidelity >}}

    - This includes ranks equivalent to or less than Guide.
    - Guides are allowed to be staff on another server, as they have no expectations with attendance.
    - If we discover that you secretly are staff on another server, you will be demoted from all staff ranks.

3. Deal with problematic players promptly. {{< anchor staff-b3-prompt-action >}}

    - If a significant number of players on the server complain about another player then an investigation should be done.
    - Don't allow the problem to continue unresolved for an extended time.

4. Do not participate in PvP or raiding without consent. {{< anchor staff-b4-pvp >}}

    - Do not circumvent this using an alternate account.
    - This includes leading a guild that participates in PvP/raiding.
    - If a player kills you or attacks you with intent to kill, you may seek vengeance within reason.
    - For your own protection, make sure you record evidence that consent was given.

## C. Administrators and Head Moderator {{< anchor staff-c-admin >}}

1. Don't mess with areas of the system that you have no business in. {{< anchor staff-c1-keep-out >}}

2. Do what is necessary to protect system security. {{< anchor staff-c2-security >}}

3. We reserve the right to reprimand players in extraordinary circumstances. {{< anchor staff-c3-last-resort >}}
