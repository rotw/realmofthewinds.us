+++
menu = "main"
title = "Ranks"
type = "single"
plainIDAnchors = false
extensionsmask = ["headerIds"]
+++

<style>
    dd > ul {
        padding-left: 0;
        list-style-type: none;
    }
    dd > ul > li {
        margin-top: 1em;
    }
    dd > ul > li::first-line {
        font-size: 1.1em;
        font-weight: bold;
    }
    dd > ul > li > ul {
        list-style-type: disc;
    }
</style>

# Player Ranks <a class="anchor" id="player"></a>

Default
: A new player whose whitelist application hasn't been accepted yet.

Player
: Everyone whose whitelist application has been accepted has this rank regardless of other ranks.

Dead
: A dead player. They must complete all three trials before they can leave the Nether and become alive again.

Alive
: A living player. If they die, they respawn in the Nether and become dead.

<!-- # Donator Ranks <a class="anchor" id="donator"></a> -->

# Staff Ranks <a class="anchor" id="staff"></a>
_See also: [Staff Rules](/rules/#staff)_

Guide
: Guides are similar to Helpers on other servers.
    - Powers
        - Review whitelist applications.
        - Kick and warn players.
        - Kill players as a punishment.
        - Temporarily mute or ban players.
        - Un-mute but not un-ban players.
        - Check player information, but not IP.
        - Vanish out of thin air.
    - Responsibilities
        - Aren't expected to have good attendance.
        - Enforce rules and help players.
        - Review whitelist applications.
        - Respond to and resolve support tickets

Junior Moderator
: Moderators are expected to be active, reliable, and trustworthy.
    - Powers
        - All powers of Guides.
        - Permanently mute, ban, or IP-ban players.
        - Check player IP.
        - View and edit player inventories.
        - Change game mode and basic cheats.
        - Full access to the teleport menu.
        - Full access to player properties.
        - Read-only access to console.
    - Responsibilities
        - All responsibilities of Guides.
        - Expected to have good attendance.
        - Keep an eye on players while vanished.
        - Inspect logged block changes and rollback griefs upon request.
        - Maintain up-to-date knowledge of Minecraft, rules, commands, plugins, etc.
        - Contribute to server projects when needed.

Senior Moderator
: A moderator who has been around for years and can be trusted with all permissions.
    - Powers
        - All powers of Junior Moderators.
        - All in-game permissions.
        - Ability to OP themselves temporarily. (No one can be OP permanently.)
        - Access to console (maybe).
    - Responsibilities
        - All responsibilities of Junior Moderators.
        - Recruit and train guides and new junior moderators.
        - Ensure staff are preforming their duties effectively.
        - Communicate staff expectations and takes disciplinary action when necessary.

Administrator <br> & Owner
: Administrators are responsible for maintaining system stability and implementing changes to the system.
    - Powers
        - All powers of Senior Moderators.
        - Full access to console.
    - Responsibilities
        - All responsibilities of Senior Moderators.
        - Provide technical support for plugins issues our players encounter
        - Manage the configuration and operation of plugins and the server
        - Monitor the system daily and respond immediately to security or usability concerns
        - Create and verify backups of data

