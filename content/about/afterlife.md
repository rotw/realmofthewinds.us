+++
title = "Afterlife"
type = "about"
image = "images/afterlife/intro.png"
featured = true
+++

When you start the game, you are imprisoned in the Nether. But don't worry, you can earn your way out by completing the Nether Trials which are a mix between quests and prison. After that, you respawn in survival, and when you die, you have to do the Nether Trials all over again.

<!--more-->

# The Nether Trials

In order to earn your freedom, you must complete the first three Nether Trials. This is kind of like Prison, except the first three are designed to be repeated every time you die.

## I. Crimson Cavern

_Gather the following items to complete this trial:_

- 64 Brown Mushroom
- 64 Red Mushroom
- 64 Quartz
- 64 Nether Wart

## II. Bloody Pigmen!

_Gather the following items to complete this trial:_

- 32 Cooked Porkchop
- 32 Rotten Flesh
- 120 Scorched Flesh
- 16 Gold Nugget
- 94 Nether Brick
- 6 Brick

## III. Desolate Fortress

_Gather the following items to complete this trial:_

- 64 Iron Ingot
- 16 Gold Ingot
- 16 Diamond
- 24 Blaze Rod
- 16 Bone
