+++
title = "Natural Protection"
type = "about"
image = "images/natural-protection/intro.png"
featured = true
+++

A custom land protection that is based on where and how much you build instead of how long you have played and what you have manually selected. All you have to do is build something, and right-click it with a piece of paper. You can use the menu to add your friends, and more.

<!--more-->

Most anti-grief plugins (like GriefPrevention) make you select the exact area that is protected using a tool like a golden shovel at best, or confusing commands at worst. They’re also either completely unlimited or limited by some completely unrelated metric like play time or virtual currency. Wouldn’t it be simpler if it would just keep track of where you’re building? Now all you have to do is tell the plugin to claim the building and invite your friends (if you want to).

# How to Claim Land

1. **Build something.** Natural Protection requires a building to protect land around it.
2. Right click with **paper**.
3. Click **New Property** (paper).
4. **Type a name** for your property in chat. Everyone will be able to read this, so **don't** name it something like "My House".

# Menu Overview

<img src="/images/natural-protection/main-menu.png" class="center-block">

1. **Property Name**: This can be changed in the property settings. If you don't know what to name it, choose something like "YourName's House".
2. **Information**: Click here to go back to your property list. This shows basic information about your property.
3. **Owner**: Shows the owner of the property. Click here to give it to someone else.
4. **Trust List**: Shows all members of the property from highest to lowest role. Click one of them to change their role or when the trust expires.
5. **Add Trust**: Trust someone to your property. Their role will be member by default. If your role is trusted, their trust will expire in 1 hour. This can be increased up to 10 days.
6. **Settings**: (Left to right)
  - **Rename property**: Click here, then type a new name in chat. If you don't know what to name it, choose something like "YourName's House".
  - **Private/Public**: Click here to toggle between public and private. Anyone is able to edit a public property; however, this still does not make it legal to grief.
  - **Mob Griefing**: Click here to toggle mob griefing on or off. When mob griefing is off, creepers cannot destroy blocks, and endermen can't pickup blocks. Farmer villagers are still able to use farms, and sheep are still able to eat grass.
  - **TNT Explosions**: Click here to toggle tnt on or off. When tnt is off, explosions from tnt will not do damage, and tnt cannot be lit.
  - **Abandon/Transfer**: Click here to open the abandon/transfer menu.

## Trust Menu

<img src="/images/natural-protection/trust-menu.png" class="center-block">

1. **Property Name**: This can be changed in the property settings. If you don't know what to name it, choose something like "YourName's House".
2. **Member Name**: The name of the member whose trust you are editing.
3. **Member Information**: Information about the member whose trust you are editing.
4. **Roles**: Click to choose a role.
  - **Member**: Has build access and nothing else.
  - **Trusted**: Can temporarily trust others.
  - **Manager**: Can trust/untrust players with a role lower than manager.
  - **Co-Owner**: Full control.
5. **Change expiration date**.
6. **Save**: Saves changes to the trust, and goes back to the property menu.
7. **Cancel**: Goes back to the property menu.

### Trust Expiriation Menu

<img src="/images/natural-protection/trust-expire-menu.png" class="center-block">

1. **Property Name**: This can be changed in the property settings. If you don't know what to name it, choose something like "YourName's House".
2. **Member Name**: The name of the member whose trust you are editing.
3. **Permanent**: Make this trust last forever.
4. **Temporary**: Set an expiration date.
5. **# Days**: Change the number of days. Left click a digit to increase. Right click a digit to decrease. Click the block to set it to 0.
6. **# Hours**: Change the number of hours. Left click a digit to increase. Right click a digit to decrease. Click the block to set it to 0.
7. **# Minutes**: Change the number of minutes. Left click a digit to increase. Right click a digit to decrease. Click the slab to set it to 0.
8. **# Seconds**: Change the number of seconds. Left click a digit to increase. Right click a digit to decrease. Click the snow layer to set it to 0.
9. **Save**: Saves changes to the trust, and goes back to the property menu.
10. **Cancel**: Goes back to the property menu.

## Abandon/Transfer Menu

<img src="/images/natural-protection/abandon-transfer-menu.png" class="center-block">

1. **Property Name**: This can be changed in the property settings. If you don't know what to name it, choose something like "YourName's House".
2. **Information**: Click here to go back to your property list. This shows basic information about your property.
3. **Abandon Land**: Abandon the land that you are standing in without deleting the property. This operation starts in the voxel you are standing in, and does a flood fill to affect connected voxels.
4. **Abandon Property**: Delete the property and abandon **all** land associated with it.
5. **Transfer Land**: Transfer this land to another property that you own. This operation starts in the voxel you are standing in, and does a flood fill to affect connected voxels.
6. **Give Ownership**: Give ownership of this property to another player.
7. **Cancel**: Goes back to the property menu.

# Technical Documentation
Most people won't need to know this stuff.

## Voxels
Each world is divided into a 3D grid of voxels, each of which can be assigned to one or zero properties. A voxel may be configured to be any size, but is currently set to be the same size as a chunk (16 x 256 x 16). Therefore, the word *voxel* may be used interchangeably with *chunk*.

### Value
Each voxel has a value which is the total value of each block in the voxel. *Natural blocks* like stone, dirt, grass, etc. have 0 value, so these will not help with claiming land. *Most blocks* have a value of 1. *Important blocks* like chests, furnaces, anvils, etc. have a value of 8. *Mineral blocks* (not ore) have a value of 4.

When a voxel is first interacted with, Natural Protection will calculate the value of the voxel, and set that as the **natural value**. This is a suboptimal solution to natural structures being claimable without players building anything. In the future, Natural Protection will use block logger data from [BlocksHub](https://www.spigotmc.org/resources/blockshub.331/) to calculate value, if it is installed.

**Artificial value** is the natural value subtracted from the total value of a voxel. A voxel needs 64 artificial value to be claimable. That's a stack of planks or 8 chests. Voxels within a radius of 1 are protected from grief and included as part of the property.

An existing property will automatically expand when a voxel within a radius of 2 becomes claimable. A voxel will be automatically unclaimed when it is no longer claimable, but it may still be protected if it is near a claimed voxel.

### Table: `np_voxels`

 - **world**: The world that the voxel exists in.
 - **x, y, z**: The coordinates of the voxel. This is **not** the same as block/entity coordinates!
 - **value**: The total value of the property.
 - **natvalue**: The portion of the value which is part of a naturally generated structure.
 - **property_id**: An integer representing a property.

## Properties
An area of protected land. Voxels don't need to be connected.

### Table: `np_properties`

 - **property_id**: An integer representing the property.
 - **name**: The property's name. May contain color codes.
 - **owner**: An integer representing the user who owns this property.
 - **icon**: The [material](https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html) to use as an icon for this property.
 - **is_admin**: 0 if a normal property, 1 if it belongs to an administrator.
 - **is_public**: 0 if a normal property, 1 to make it public and disable all protections.
 - **allow_tnt**: 0 to cancel TNT explosions, 1 to allow TNT explosions.
 - **allow_mobgrief**: 0 to cancel mob griefing, 1 to allow mob griefing.
 - **allow_escape**: 0 to always allow teleports, 1 to prevent non-members from teleporting away.

## Users
Most users are players, but it is also possible to add a "fake" user. This is useful for NPCs or testing the plugin without friends. :(

### Table: `np_users`

 - **user_id**: An integer representing the user.
 - **uuid**: The player's UUID given by Mojang, if this user is a player.
 - **name**: The user's name.
 - **is_fake**: 0 if a player, 1 if a fake user (NPC).
 - **skinname**: The name to use to load a skin.

## Trusts
An object which allows a player to access a property. The owner of a property does not need a trust. (See also: Roles)

### Table: `np_trusts`

 - **user_id**: An integer representing a user.
 - **property_id**: An integer representing a property.
 - **role**: The role assigned to this player (non-owner).
 - **date_added**: The date and time that this trust was created or edited.
 - **date_expire**: The date and time that this trust will expire.