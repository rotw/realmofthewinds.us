+++
title = "Raiding & PvP"
type = "about"
image = "images/raiding-and-pvp/intro.png"
featured = true
+++

Most servers either allow griefing (Factions, Anarchy, etc.) or allow you to steal items from the few chests that aren't protected. On Realm of the Winds, you can steal and kill so long as you can figure out how to get to the chests without breaking any rules (mainly, no griefing).

<!--more-->

# What needs to be protected?

- Yourself, and your friends.
- Chests and other containers with items you don't want to use.
- Beds so that raiders can't set home.
- Animals, villagers, and other mobs.
- Items in item frames.
- Anything else you don't want raiders to access.

# How to Keep Out Raiders

Raiders are not allowed to break blocks, so take advantage of this to protect your stuff. The simplest option is to put blocks on top of your chests and/or in front of your doors. But you have to remember to put the blocks back when you break them. It's also ugly and boring.

You can use redstone to protect your stuff more easily. There are nearly infinite possible ways to do this, but they generally fall into two categories: puzzles and fully secure.

## Puzzles

These don't make your base completely secure, but if complex enough, it would be so difficult to gain entry that most raiders will stop trying. Most redstone security ideas found online fall under this category.

## Fully Secure

If you make no mistakes with any of these, it would be theoretically impossible to raid your base. However, those with affinity for the art of stealth can find a way.

### One-way Door

1. Put your stuff inside a room with one exit. This exit could be an iron door, piston door, or anything that can only be opened by redstone.
2. Place a pressure plate or any other redstone input only accessible from inside the room.
3. Connect the input to the door.
4. Place a bed inside the room, and set your home there.

Now you can teleport with your compass to go inside the room. A downside is that if raiders are able to sneak inside, they can set home in your bed.

### Observers

Instead of having several lock blocks that need to be kept track of, you can use redstone controlled by a single lock block.

1. Pick a convenient place to put a block to use as a lock.
2. Place an observer for detecting when this block is broken or placed.
3. Connect the observer to a T flip-flop.
4. Connect the T flip-flop to sticky pistons that move blocks over your chests, and anything else you need.
5. If you set this up correctly, everything should be locked when a block is placed in front of the observer, and unlocked when the block is broken.

# How to Raid Legally

**[Make sure you read all of the game rules before you begin raiding!](/rules/#game)** This is not a factions server, so many strategies you would use there would be illegal here. The basic idea, is that you are not allowed to break blocks unless it's on the exceptions list.

First you need to find a base that can be raided. This means a lot of exploring. Use a stick to see if land is claimed. *Do not* break or place blocks to test this, because staff and property members will be notified when you do this. Staff may also suspect that you are trying to grief.

Once you have found a base, look around without getting caught or trapped. Always make sure you have an exit plan. Using teleportation to escape enemy properties is disabled by default. If you are trapped, you can use `/suicide` as a last resort.

Most bases are just simple houses. Most of those houses haven't been protected because the builder is new. You can choose to be nice and teach them how to claim land and protect their items. Or, you can just kill them and take their stuff. But, if they leave the server because of this, that would mean less players on the server which means less to raid.

If the base you found is above average in size or quality, you should be more careful. I would recommend keeping your valuable gear at home or in your ender chest while you are scouting the base. If the builders are online, invisibility potions are your friend. Make sure you are looking for ways to get out while you are looking for ways to get in.

Once you have a plan for the raid you may choose to go back home to gear up and gather reinforcements if needed. I am not going to get into specific strategies here as this would depend on the base. Just be careful, follow the rules, and have fun.