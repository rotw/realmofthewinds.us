+++
title = "Warp Menu"
type = "about"
image = "images/warp-menu/intro.png"
featured = true
+++

No cheaty commands for teleports. Instead, you craft a compass which presents an easy-to-use menu. The server is split into multiple realms, and your location in each realm will be saved. Instead of /home and /sethome, use a bed to set home, and a compass to teleport.

<!--more-->

# Realms

The Nether
: The hellish wasteland that everyone goes to when they die.  
  _See also: [Afterlife](/about/afterlife/)._

Spawn
: A castle in the sky where you appear upon resurrection. This acts as a hub for trade and travel between realms.

Creative
: A secret realm for staff members to build structures for the server.

Overworld
: A regular survival world.

Amplified
: A survival world with amplified world generation.

The End
: The place where you go to slay the Ender Dragon and raid End Cities. Besides that, it's a boring empty wasteland.

# Public Warps

Public warps exist to make travel to public places easy. To be eligible for a public warp, the place must be beneficial to the server as a whole. Public warps may be added or removed at any time by staff discretion. The following are examples of eligible places:

- Active towns/cities that are open to applications.
- Farms/grinders for public use. (May include a fee.)
- Shops that are not out of stock.
- Fully functional mini-games.
- Extraordinary builds fully open to exploration.

## How to Apply

Type `/apply warp` in-game. Make sure you read all of the terms, and have the necessary fee and information prepared.
